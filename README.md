# IpInfo API Sample Usage and Implementation

## Introduction

this is a sample project on how to utilize IpInfo API. 

current usage include google maps for visual representation of latitude and longitude, and DarkSky
for weather status on the current location.

## Installation using Composer

The easiest way to install project project is to use
[Composer](https://getcomposer.org/).  If you don't have it already installed,
then please install as per the [documentation](https://getcomposer.org/doc/00-intro.md).

To preview this current project:

1. download/clone repository via git which will be provided via email
2. install the project on a web server (virtualbox / xampp)
3. see additional details below for example virtualhost config for apache

```bash
$ composer install
```

## Development mode

The skeleton ships with [zf-development-mode](https://github.com/zfcampus/zf-development-mode)
by default, and provides three aliases for consuming the script it ships with:

```bash
$ composer development-enable  # enable development mode
$ composer development-disable # disable development mode
$ composer development-status  # whether or not development mode is enabled
```

You may provide development-only modules and bootstrap-level configuration in
`config/development.config.php.dist`, and development-only application
configuration in `config/autoload/development.local.php.dist`. Enabling
development mode will copy these files to versions removing the `.dist` suffix,
while disabling development mode will remove those copies.

Development mode is automatically enabled as part of the skeleton installation process. 
After making changes to one of the above-mentioned `.dist` configuration files you will
either need to disable then enable development mode for the changes to take effect,
or manually make matching updates to the `.dist`-less copies of those files.


> ### VirtualBox
>
> The vagrant image is based on ubuntu/xenial64. If you are using VirtualBox as
> a provider, you will need:
>
> - Vagrant 1.8.5 or later
> - VirtualBox 5.0.26 or later

For vagrant documentation, please refer to [vagrantup.com](https://www.vagrantup.com/)


## Web server setup

### Apache setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

```apache
<VirtualHost *:80>
        ServerName s1.taivara
        DocumentRoot /var/www/html/taivara/public
        #SetEnv "APP_ENV" "development"

        ErrorLog ${APACHE_LOG_DIR}/taivara-error.log
        CustomLog ${APACHE_LOG_DIR}/taivara-access.log combined

        <Directory /var/www/html/taivara/public>
                DirectoryIndex index.php
                AllowOverride All
                Order allow,deny
                Allow from all

                <IfModule mod_authz_core.c>
                         Require all granted
                </IfModule>
        </Directory>
</VirtualHost>
```

### Configurations 

update the configuration keys as necessary. I have included my test access key for this project for demo purposes.

location: project_root/config/autoload/local.php
~~~~~
<?php

return [
    'ipinfo_token' => 'IP_INFO_TOKEN_HERE',
    'darksky_key' => 'DARKSKY_KEY_HERE'
];
~~~~~

Restart the nginx, now you should be ready to go!

