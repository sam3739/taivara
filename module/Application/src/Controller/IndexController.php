<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
//        //49.144.143.136
        $tmpl       = $this->customTemplate();
        $ipInfo     = $this->ipInfoPlugin();
        $idDetails  = $ipInfo->getDetails();


        $params = [];

        if (!isset($idDetails->bogon)) {
            $weather = $this->darkSkyPlugin();

            $weatherDetails = $weather->getCurrentForecast($idDetails->latitude, $idDetails->longitude);


            $params = [
                'ipDetails' => $idDetails->all,
                'weatherDetails' => $weatherDetails
            ];

            return $tmpl->partialLayout($params, '/application/index/index.phtml');
        } else {
            return $tmpl->partialLayout($params, '/error/404.phtml');
        }
    }
}
