<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class TemplatePlugin extends AbstractPlugin
{

    protected $viewrenderer;

    public function __construct($viewrenderer)
    {

        $this->viewrenderer = $viewrenderer;
    }

    public function testPlugin() {
        echo "Hello";
        exit;
    }


    public function partialLayout($data = [], $template = "", $ajax = null)
    {
        $ajax = (! empty($ajax)) ? $ajax : false;

        $view = new ViewModel($data);
        $view->setTerminal($ajax)->setTemplate($template);

        return ($ajax == true) ? $this->viewrenderer->render($view) : $view;
    }

    public function currentLocation()
    {

        $location = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]/";
        return $location;
    }

    public function rootLocation()
    {

        $location = "http://$_SERVER[HTTP_HOST]/";
        return $location;
    }

    public function jsonResponse($variables)
    {
        $jsonModel = new JsonModel();

        $jsonModel->setVariables($variables);

        return $jsonModel;
    }
}
