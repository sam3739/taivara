<?php

namespace Application\Controller\Plugin;


use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use ipinfo\ipinfo\IPinfo;

class IpInfoPlugin extends AbstractPlugin
{
    protected $ipInfoToken;

    public function __construct(string $ipInfoToken)
    {
        $this->ipInfoToken = $ipInfoToken;
    }

    public function getToken()
    {
        return $this->ipInfoToken;
    }


    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function getDetails()
    {
        $client = new IPinfo($this->ipInfoToken);
        $details = $client->getDetails('49.144.143.136');
        //$details = $client->getDetails($this->getUserIpAddr());
        return $details;
    }



}