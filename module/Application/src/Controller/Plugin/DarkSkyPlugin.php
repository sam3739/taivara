<?php

namespace Application\Controller\Plugin;


use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use DmitryIvanov\DarkSkyApi\DarkSkyApi;

class DarkSkyPlugin extends AbstractPlugin
{
    protected $darkSkyKey;

    public function __construct(string $darkSkyKey)
    {
        $this->darkSkyKey = $darkSkyKey;
    }

    public function getKey()
    {
        return $this->darkSkyKey;
    }

    public function getCurrentForecast($lat, $long)
    {
        $forecast = (new DarkSkyApi($this->darkSkyKey))
        ->location($lat, $long)
        ->units('us')
        ->language('en')
        ->forecast('currently');

        return $forecast->currently();
    }

}