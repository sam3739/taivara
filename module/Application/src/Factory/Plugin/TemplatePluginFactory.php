<?php

namespace Application\Factory\Plugin;

use Application\Controller\Plugin\TemplatePlugin;
use Interop\Container\ContainerInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\ServiceManager\Factory\FactoryInterface;

class TemplatePluginFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return ListController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $viewrenderer = $container->get(PhpRenderer::class);
        
        return new TemplatePlugin($viewrenderer);
    }
}